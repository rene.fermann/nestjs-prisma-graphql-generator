import { generate } from "./generate";

describe("nestPrismaGraphqlGenerator", () => {
  it("should work", () => {
    expect(generate()).toEqual("nest-prisma-graphql-generator");
  });
});
